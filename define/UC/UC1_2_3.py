import pandas as pd
import time
import datetime

import logging
logging.basicConfig(filename="logging_data/rasa_chatlog_processor_log",
                    format='%(asctime)s %(message)s',
                    filemode='w')
logger = logging.getLogger()
logger.setLevel(logging.INFO)
import unicodedata
# from pyvi import ViTokenizer, ViPosTagger
import os
script_dir = os.path.dirname(__file__)

with open(script_dir + "/obj_type", "r", encoding="utf-8") as obj_type_file:
    lines = obj_type_file.readlines()
    objtype_list = [x.strip() for x in lines]

def specify_uc1_uc2_uc3(rasa_chatlog_df: pd.DataFrame):
        logger.info("Specify uc for conversations")
        start_time = time.time()
        conversation_ids = rasa_chatlog_df["conversation_id"].drop_duplicates(keep="first").to_list()
        rasa_chatlog_df.insert(2, "use_case", "")
        for id in conversation_ids:
            chatlog_sub_df = rasa_chatlog_df[rasa_chatlog_df["conversation_id"] == id]
            chatlog_sub_df_first_turn = chatlog_sub_df[chatlog_sub_df["turn"].isin([0, 1])]
            # conversation_attachments = chatlog_sub_df['attachments'].to_list()
            conversation_attachments = chatlog_sub_df_first_turn['attachments'].to_list()
            index_list = chatlog_sub_df_first_turn.index.tolist()
            conversation_has_images = False
            if any("scontent" in str(x) for x in conversation_attachments):
                conversation_has_images = True

            for i, item_index in enumerate(index_list):
                user_message = chatlog_sub_df_first_turn.loc[item_index, "user_message"]
                if str(chatlog_sub_df_first_turn.loc[item_index, "entities"]) != "nan":
                    entities_list = chatlog_sub_df_first_turn.loc[item_index, "entities"].split(",")
                    if any("price" in str(x) for x in entities_list):
                        if conversation_has_images:
                            rasa_chatlog_df.at[item_index, "use_case"] = "uc_s2"
                            break
                        else:
                            break
                    if str(user_message) != "nan":
                        user_message_correction = chatlog_sub_df_first_turn.loc[item_index, "user_message_correction"]

                        # message_pos_tag = ViPosTagger.postagging(ViTokenizer.tokenize(user_message_correction))
                        # words = [x for x in message_pos_tag[0]]
                        # pos = [x for x in message_pos_tag[1]]
                        #
                        # con_x_khong_form = False
                        # co_x_khong_form = False
                        # if "còn" in words and "không" in words:
                        #     con_index = words.index("còn")
                        #     khong_index = words.index("không")
                        #     if con_index < khong_index:
                        #         in_between_word_pos = pos[con_index:khong_index]
                        #         """
                        #         N - Common noun
                        #         Nc - Noun Classifier
                        #         Ny - Noun abbreviation
                        #         Np - Proper noun
                        #         Nu - Unit noun
                        #         """
                        #         if any(x in in_between_word_pos for x in ["N", "Nc", "Ny", "Np", "Nu"]):
                        #             con_x_khong_form = True
                        # elif "có" in words and "không" in words:
                        #     co_index = words.index("có")
                        #     khong_index = words.index("không")
                        #     if co_index < khong_index:
                        #         in_between_word_pos = pos[co_index:khong_index]
                        #         if any(x in in_between_word_pos for x in ["N", "Nc", "Ny", "Np", "Nu"]):
                        #             co_x_khong_form = True

                        # if conversation_has_images and (con_x_khong_form or "còn không" in user_message_correction or all(x in user_message_correction for x in ["còn", "không"])):
                        if conversation_has_images and ("còn không" in user_message_correction or all(
                                x in user_message_correction for x in ["còn", "không"])):
                            rasa_chatlog_df.at[item_index, "use_case"] = "uc_s1"
                            break
                        # elif not conversation_has_images and (co_x_khong_form or "có không" in user_message_correction or all(x in user_message_correction for x in ["có", "không"])):
                        elif not conversation_has_images and ("có không" in user_message_correction or all(
                                x in user_message_correction for x in ["có", "không"])):
                            if str(chatlog_sub_df_first_turn.loc[item_index, "entities"]) != "nan":
                                entities_list = chatlog_sub_df_first_turn.loc[item_index, "entities"].split(",")
                                entities_list = [x for x in entities_list if x != '']
                                if any(x in objtype_list for x in entities_list):
                                    if len(entities_list) == 1:
                                        rasa_chatlog_df.at[item_index, "use_case"] = "uc_s31"
                                        break
                                    else:
                                        rasa_chatlog_df.at[item_index, "use_case"] = "uc_s32"
                                        break
        logger.info("Specify usecases: --- %s seconds ---" % (time.time() - start_time))
        return rasa_chatlog_df