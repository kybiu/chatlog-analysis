import pandas as pd
import time
import datetime
import unicodedata

import logging
logging.basicConfig(filename="logging_data/rasa_chatlog_processor_log",
                    format='%(asctime)s %(message)s',
                    filemode='w')
logger = logging.getLogger()
logger.setLevel(logging.INFO)


def specify_uc4(chatlog_df, conversation_turn_pair_without_img, model_ner):
        start_time = time.time()
        # chatlog_df.insert(2, "uc4", "")
        attributes = ["age_of_use", "guarantee", "color", "material", "origin", "promotion", "size", "weight", "brand",
                      "price"]
        CON = ['còn', ' con ', ' conf ']
        KHONG = ['không', ' k ', ' ko ', ' hem ', ' hok ', ' khg ', 'khoong']

        uc4_base_require_entities = ["object_type", "mention"]

        filter_word = ["ship", "shopee", "shoppee", "freeship", "shope"]
        conversation_first_turn_pair_without_img = [x for x in conversation_turn_pair_without_img if x[1] in [0, 1]]
        for pair in conversation_first_turn_pair_without_img:
            conversation_id = pair[0]
            turn = pair[1]
            sub_df_full_conversation = chatlog_df[chatlog_df["conversation_id"] == conversation_id]
            list_uc = [x for x in sub_df_full_conversation['use_case'].to_list() if str(x) not in ['', 'nan']]
            if all(x not in list_uc for x in ["uc_s1", "uc_s2"]):
                sub_df = chatlog_df[(chatlog_df["conversation_id"] == conversation_id) & (chatlog_df["turn"] == turn) & (
                    ~chatlog_df["user_message"].isin(["nan", "user"]))].reset_index()
                if len(sub_df) > 0:
                    for item_index in range(0, len(sub_df)):
                        user_message = sub_df.at[item_index, "user_message_correction"]
                        user_message = unicodedata.normalize("NFC", str(user_message))
                        user_message = user_message.lower()
                        message_index = sub_df.at[item_index, "index"]
                        con = [word for word in CON if (word in user_message)]
                        khong = [word for word in KHONG if (word in user_message)]

                        if str(user_message) in ["nan", "user", "", " "]:
                            continue
                        ner_output = model_ner.process(sample=user_message)
                        entities = [x["entity"] for x in ner_output]
                        prod_attribute = [x["entity"] for x in ner_output if x["entity"] in attributes]

                        if con and khong and entities:
                            # if any(x in entities for x in uc4_base_require_entities) and all(x not in user_message for x in filter_word):
                            if any(x in entities for x in uc4_base_require_entities):
                                if len(prod_attribute) == 0:
                                    chatlog_df.at[message_index, "use_case"] = "uc_s41"
                                elif len(prod_attribute) == 1:
                                    chatlog_df.at[message_index, "use_case"] = "uc_s42"
                                else:
                                    chatlog_df.at[message_index, "use_case"] = "uc_s43"
        logger.info("Specify UC4: " + str(time.time() - start_time))
        return chatlog_df