import unicodedata


def specify_uc9(chatlog_df, conversation_turn_pair_without_img):
    ORDER = ['lấy', 'để', 'order', 'bán']
    BAO_NHIEU = ['bao nhiêu', 'phí', '0', 'tới', 'đến', 'về']
    CAM_THAN = ['nhé', 'nha']
    SHIP = ['ship cho mình']

    for pair in conversation_turn_pair_without_img:
        conversation_id = pair[0]
        turn = pair[1]
        sub_df_full_conversation = chatlog_df[chatlog_df["conversation_id"] == conversation_id]
        list_uc = [x for x in sub_df_full_conversation['use_case'].to_list() if str(x) not in ['', 'nan']]
        if all(x not in list_uc for x in ["uc_s1", "uc_s2", "uc_s4", "uc_s5", "uc_s8"]):
            sub_df = chatlog_df[(chatlog_df["conversation_id"] == conversation_id) & (chatlog_df["turn"] == turn) & (
                ~chatlog_df["user_message"].isin(["nan", "user", '']))].reset_index()

            for index, item in sub_df.iterrows():
                user_message = item["user_message_correction"]
                if str(user_message) == "nan" or str(user_message) == '':
                    user_message = "nan"
                    continue
                user_message_correction = unicodedata.normalize("NFC", user_message)
                user_message_correction = user_message_correction.lower()
                edit_message = user_message_correction
                order = [key for key in ORDER if (key in edit_message)]
                bao_nhieu = [key for key in BAO_NHIEU if (key in edit_message)]
                cam_than = [key for key in CAM_THAN if (key in edit_message)]
                ship = [key for key in SHIP if (key in edit_message)]

                message_index = sub_df.at[index, "index"]
                if (len(order) > 0 and len(bao_nhieu) == 0 and len(cam_than) > 0) or (
                        len(ship) > 0 and len(bao_nhieu) == 0):
                    if turn == 0:
                        chatlog_df.at[message_index, "use_case"] = "uc_s9"
                        continue
                    elif turn == 1:
                        chatlog_df.at[message_index, "use_case"] = "uc_s9"
                        continue
                    elif turn > 1:
                        chatlog_df.at[message_index, "use_case"] = "uc_s9"
                        continue

    return chatlog_df
