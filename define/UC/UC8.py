import re
from underthesea import pos_tag


def specify_uc8(chatlog_df, conversation_turn_pair_without_img):
    SHIP = ["ship"]
    BAO_NHIEU = ["bao nhiêu", "phí", "hỗ trợ", "giá", "sao", "bao tiền"]
    LOCA = ["ở", "về", "khu vực"]
    FREESHIP = ["freeship"]
    TOKEN = ['Np']
    turn_ship = []
    for pair in conversation_turn_pair_without_img:
        conversation_id = pair[0]
        turn = pair[1]
        sub_df_full_conversation = chatlog_df[chatlog_df["conversation_id"] == conversation_id]
        list_uc = [x for x in sub_df_full_conversation['use_case'].to_list() if str(x) not in ['', 'nan']]
        if all(x not in list_uc for x in ["uc_s1", "uc_s2", "uc_s4", "uc_s5"]):
            mess_user = ''
            sub_df = chatlog_df[(chatlog_df["conversation_id"] == conversation_id) & (chatlog_df["turn"] == turn) & (
                ~chatlog_df["user_message"].isin(["nan", "user",'']))].reset_index()
            if len(sub_df) > 0:
                for index, item in sub_df.iterrows():
                    if str(item['user_message_correction']) != 'nan' and str(item['user_message_correction']) != 'user':
                        mess_user += (str(item['user_message_correction']) + ' . ')

                mess_user = mess_user.replace('\n', ' ')
                edit_mess = mess_user
                ship = [key_ship for key_ship in SHIP if (key_ship in edit_mess)]
                bao_nhieu = [key for key in BAO_NHIEU if (key in edit_mess)]
                freeship = [key for key in FREESHIP if (key in edit_mess)]
                loca = [key for key in LOCA if (key in edit_mess)]
                message_index = sub_df.at[0, "index"]

                if ((len(ship) > 0 and len(bao_nhieu) > 0)) or len(freeship) > 0:
                    pos_token = pos_tag(mess_user)
                    location = [key for key in pos_token if (key[1] in TOKEN)]
                    if len(location) > 0:
                        for l in location:
                            if len(l[0]) > 4:
                                chatlog_df.at[message_index, "use_case"] = "uc_s8"
                                break
                    elif len(loca) > 0:
                        chatlog_df.at[message_index, "use_case"] = "uc_s8"
    return chatlog_df
