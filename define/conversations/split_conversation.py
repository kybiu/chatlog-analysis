import pandas as pd
import time
import datetime

import logging
logging.basicConfig(filename="logging_data/rasa_chatlog_processor_log",
                    format='%(asctime)s %(message)s',
                    filemode='w')
logger = logging.getLogger()
logger.setLevel(logging.INFO)


def split_chatlog_to_conversations(rasa_chatlog_df: pd.DataFrame, begin_conversation_id=0):
        """
       Split chatlog to conversation
       :param fb_conversations:
       :return:
       """
        start_time = time.time()
        logger.info("Split chatlog to conversations")
        rasa_chatlog_df.insert(0, 'conversation_id', 0)
        rasa_chatlog_df.insert(9, 'conversation_begin_date', None)
        rasa_chatlog_df.insert(10, 'conversation_begin_time', None)

        fmt = '%Y-%m-%d %H:%M:%S'

        # sender_ids = list(rasa_chatlog_df["sender_id"].dropna())
        sender_ids = []
        ids = list(rasa_chatlog_df["sender_id"].dropna())
        
        for i, sender_id in enumerate(ids):
            if sender_id == 1454523434857990:
                try:
                    sender_ids.append((list(rasa_chatlog_df["to_sender_id"].dropna()))[i])
                except:
                    pass
            else:
                sender_ids.append(sender_id)

        sender_ids = sorted(set(sender_ids), key=sender_ids.index)

        conversation_id = begin_conversation_id
        last_conversation_id = None
        conversation_begin_date = None
        conversation_begin_time = None
        checked_sender_id = []
        for sender_id_index, sender_id in enumerate(sender_ids):
            if sender_id not in checked_sender_id:
                conversation_id += 1
                checked_sender_id.append(sender_id)

            try :
                a = rasa_chatlog_df["to_sender_id"]
                sub_df = rasa_chatlog_df[(rasa_chatlog_df["sender_id"] == sender_id) | (rasa_chatlog_df["to_sender_id"] == sender_id)].reset_index()
            except:
                sub_df = rasa_chatlog_df[rasa_chatlog_df["sender_id"] == sender_id].reset_index()
            
            for index, item in sub_df.iterrows():
                if last_conversation_id is None or last_conversation_id != conversation_id:
                    conversation_begin_date = datetime.datetime.strptime(item["created_time"][:10], "%Y-%m-%d")
                    conversation_begin_time = datetime.datetime.strptime(item["created_time"][11:], "%H:%M:%S")
                message_index = item["index"]
                rasa_chatlog_df.at[message_index, "conversation_id"] = conversation_id
                rasa_chatlog_df.at[message_index, "conversation_begin_date"] = conversation_begin_date
                rasa_chatlog_df.at[message_index, "conversation_begin_time"] = conversation_begin_time

                last_conversation_id = conversation_id

                if index + 1 < len(sub_df):
                    next_message = sub_df.iloc[index + 1]
                    current_time = item["created_time"][:10] + " " + item["created_time"][11:19]
                    current_time = datetime.datetime.strptime(current_time, fmt)

                    next_time = next_message["created_time"][:10] + " " + next_message["created_time"][11:19]
                    next_time = datetime.datetime.strptime(next_time, fmt)

                    time_diff = (next_time - current_time).total_seconds()
                    if (current_time.weekday() > 3) and (time_diff > 259200):
                        conversation_id += 1
                    elif (current_time.weekday() < 4) and (time_diff > 172800):
                        conversation_id += 1
                        
        logger.info("Split to conversations: --- %s seconds ---" % (time.time() - start_time))
        return rasa_chatlog_df

def split_chatlog_conversations_to_turns(rasa_chatlog_df: pd.DataFrame):
        """
        Split conversations to turns
        :param rasa_chatlog_df:
        :return:
        """
        start_time = time.time()
        logger.info("Split conversations to turns")
        rasa_chatlog_df.insert(1, "turn", "")
        conversation_ids = list(rasa_chatlog_df["conversation_id"])
        conversation_ids = list(dict.fromkeys(conversation_ids))
        for id in conversation_ids:
            sub_df = rasa_chatlog_df[rasa_chatlog_df["conversation_id"] == id]
            turn = 0
            previous_index = 0
            first_item_in_sub_df = True
            for index, item in sub_df.iterrows():
                if not first_item_in_sub_df:
                    previous_sender_name = sub_df.at[previous_index, "sender"]
                    current_sender_name = item["sender"]
                    try:
                        if previous_sender_name == 'bot' and current_sender_name != previous_sender_name:
                            turn += 1
                    except:
                        a = 0
                first_item_in_sub_df = False
                previous_index = index
                rasa_chatlog_df.at[index, "turn"] = turn
        logger.info("Split conversations to turns: --- %s seconds ---" % (time.time() - start_time))
        return rasa_chatlog_df