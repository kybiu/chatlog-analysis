from csv import DictWriter
import pandas as pd
import os
import json
from datetime import date, timedelta
import datetime
from ast import literal_eval
from pymongo import MongoClient
from data_cleaning import *
# from rasa_chatlog_processor import RasaChalogProcessor
import logging
from crawl_rasa_all import *
logging.root.setLevel(logging.NOTSET)
logging.basicConfig(
    level=logging.NOTSET,
    format='%(asctime)s %(module)s - %(funcName)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)
logger = logging.getLogger(__name__)

db_name = "rasa_chatlog_all_14_9"


def crawl_weekly():
    all_date_last_week = [date.today() - datetime.timedelta(days=x) for x in range(1, 8)]
    all_date_last_week = [str(x) for x in all_date_last_week]
    all_conversations = export_conversations(all_date_last_week[-1], all_date_last_week[0])
    all_conversation_detail = export_conversation_detail(all_conversations)
    rasa_chatlog_processed = process_raw_rasa_chatlog(all_conversation_detail, all_date_last_week)
    rasa_chatlog_clean = clean_rasa_chatlog(rasa_chatlog_processed)
    rasa_chatlog_clean = rasa_chatlog_clean.sort_values(by=["created_time"])
    try:
        last_conversation_id = get_last_document_from_db()
    except:
        last_conversation_id = 0
    processor = RasaChalogProcessor()
    rasa_chatlog = processor.process_rasa_chatlog(rasa_chatlog_clean, last_conversation_id)
    rasa_chatlog[rasa_chatlog["user_message"] != rasa_chatlog["bot_message"]]
    # upload_all_rasa_chatlog_to_atlas_mongodb(rasa_chatlog)


crawl_weekly()
