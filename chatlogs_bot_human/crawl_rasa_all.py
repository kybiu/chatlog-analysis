from csv import DictWriter
import pandas as pd
import os
import json
from datetime import date, timedelta
import datetime
from ast import literal_eval
from pymongo import MongoClient
import sys
from data_cleaning import *
from rasa_chatlog_processor import RasaChalogProcessor
import logging

logging.root.setLevel(logging.NOTSET)
logging.basicConfig(
    level=logging.NOTSET,
    format='%(asctime)s %(module)s - %(funcName)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)
logger = logging.getLogger(__name__)
db_name = "rasa_chatlog_4_11"


def get_timestamp(timestamp: int, format: str):
    """

    :param timestamp:
    :param format: %Y-%m-%d %H:%M:%S
    :return:
    """
    readable_timestamp = datetime.datetime.fromtimestamp(timestamp).strftime(format)
    return readable_timestamp


def upload_all_rasa_chatlog_to_atlas_mongodb(chalog_all):
    chatlog_rasa = chalog_all
    date_list = [datetime.datetime.strptime(x, "%Y-%m-%d %H:%M:%S") for x in list(chatlog_rasa["created_time"])]
    time_list = [datetime.datetime.strptime(x[11:], "%H:%M:%S") for x in list(chatlog_rasa["created_time"])]
    week_day = [datetime.datetime.strptime(x[:10], "%Y-%m-%d").weekday() for x in list(chatlog_rasa["created_time"])]

    chatlog_rasa['conversation_begin_time'] = pd.to_datetime(chatlog_rasa['conversation_begin_time'])
    chatlog_rasa['conversation_begin_date'] = pd.to_datetime(chatlog_rasa['conversation_begin_date'])
    if "_id" in chatlog_rasa:
        chatlog_rasa = chatlog_rasa.drop(columns=['_id'])
    chatlog_rasa.insert(0, "_id", list(range(0, len(chatlog_rasa))))
    chatlog_rasa.insert(9, "date", date_list)
    chatlog_rasa.insert(10, "conversation_time", time_list)
    chatlog_rasa.insert(11, "week_day", week_day)

    # Connect to MongoDB
    client = MongoClient("mongodb+srv://ducanh:1234@ducanh.sa1mn.gcp.mongodb.net/<dbname>?retryWrites=true&w=majority")
    db = client['chatlog_db']
    collection = db[db_name]
    data_dict = chatlog_rasa.to_dict("records")

    # Insert collection
    collection.insert_many(data_dict)


def process_raw_rasa_chatlog(rasa_chatlog_weekly: pd.DataFrame, date_list=None, year_list=None):
    logger.info("Get chatlog by month")
    if date_list is None:
        date_list = []
    if year_list is None:
        year_list = []
    rasa_conversation = rasa_chatlog_weekly
    df_data = {
        "message_id": [],
        "sender_id": [],
        "sender": [],
        "user_message": [],
        "bot_message": [],
        "utter_name": [],
        "intent": [],
        "entities": [],
        "created_time": [],
        "attachments": [],
    }
    fmt = "%Y-%m-%d %H:%M:%S"
    for rasa_index, item in rasa_conversation.iterrows():
        if item["events"] is not None and str(item["events"]) != "nan":
            events = item["events"]
            if not isinstance(item["events"], list):
                events = literal_eval(item["events"])
        else:
            continue
        sender_id = item["sender_id"]
        # Get user and bot event
        # user_bot_events = [x for x in events if x["event"] == "user" or x["event"] == "bot" or x["event"] == "action"]
        user_bot_events = [x for x in events if x["event"] == "user" or x["event"] == "bot"]
        user_action = ["none"]
        bot_action = ["none"]
        for event_index, event in enumerate(user_bot_events):

            timestamp = get_timestamp(int(event["timestamp"]), fmt)
            timestamp_month = get_timestamp(int(event["timestamp"]), "%m")
            timestamp_year = get_timestamp(int(event["timestamp"]), "%Y")
            timestamp_date = get_timestamp(int(event["timestamp"]), "%Y-%m-%d")
            message_id = ""
            user_intent = ""
            # if timestamp_month == input_month:
            if timestamp_date in date_list or timestamp_year in year_list:
                entity_list = ""
                if "metadata" in event:
                    if "utter_name" in event["metadata"]:
                        df_data["utter_name"].append(event["metadata"]["utter_name"])
                    else:
                        df_data["utter_name"].append("")
                else:
                    df_data["utter_name"].append("")

                if "parse_data" in event:
                    if "entities" in event["parse_data"]:
                        entities = event["parse_data"]["entities"]
                        if entities:
                            for item in entities:
                                if "value" in item:
                                    if item["value"] is not None:
                                        entity_list += str(item["value"]) + ","
                    if "intent" in event["parse_data"]:
                        if "name" in event["parse_data"]["intent"]:
                            user_intent = event['parse_data']['intent']['name']

                if "message_id" in event:
                    message_id = event["message_id"]

                message = event["text"]
                attachments = ""
                if message is None:
                    message = ""

                if "scontent" in message:
                    messsage_list = message.split("\n")
                    text_message = ""
                    for item in messsage_list:
                        if "scontent" in item:
                            attachments += item + ", "
                        else:
                            text_message += item + " "
                    message = text_message

                # if action_to_add is not None:
                #     df_data["action"].append(action_to_add)

                df_data["entities"].append(entity_list)
                df_data["sender"].append(event["event"])
                df_data["intent"].append(user_intent)
                df_data["message_id"].append(message_id)
                df_data["sender_id"].append(sender_id)
                df_data["created_time"].append(timestamp)
                df_data["attachments"].append(attachments)

                event_owner = event["event"]
                if event_owner == "user":
                    df_data["user_message"].append(message)
                    df_data["bot_message"].append("")
                else:
                    df_data["user_message"].append("")
                    df_data["bot_message"].append(message)

    rasa_chatlog_df = pd.DataFrame.from_dict(df_data)

    return rasa_chatlog_df


def export_conversations(start=None, end=None):
    logger.info("Export conversation")
    # conversation_api = "curl -H \"Authorization: Bearer $TOKEN\" -s https://babeshop.ftech.ai/api/conversations?limit=20&offset=0&start=2020-10-11T17:00:00.000Z"
    if start is None and end is None:
        conversation_api = "curl -H \"Authorization: Bearer $TOKEN\" -s https://babeshop.ftech.ai/api/conversations"
    else:
        conversation_api = "curl -H \"Authorization: Bearer $TOKEN\" -s https://babeshop.ftech.ai/api/conversations?start={start}T17:00:00.000Z&until={end}T17:00:00.000Z"
        conversation_api = conversation_api.format(start=str(start), end=str(end))

    token = "TOKEN=$(curl -s https://babeshop.ftech.ai/api/auth -d '{\"username\": \"me\", \"password\": \"w4J6OObi996nDGcQ4mlYNK4F\"}' | jq -r .access_token)"
    # "https://babeshop.ftech.ai/api/conversations?limit=20&offset=0&tart=2020-08-18T17:00:00.000Z&until=2020-08-19T17:00:00.000Z&intent=&entity=&action=&policies=&tags_any=&exclude_self=true&with_tags=true"
    try:
        all_conversations = json.loads(os.popen(token + " && " + conversation_api).read())
    except:
        all_conversations = None
    return all_conversations


def export_conversation_detail(all_conv):
    logger.info("Export conversation detail")

    all_sender_id = [x["sender_id"] for x in all_conv]
    counter = 0
    conversation_detail_list = []
    for sender_id in all_sender_id:
        counter += 1
        conversation_detail_api = "curl -H \"Authorization: Bearer $TOKEN\" -s https://babeshop.ftech.ai/api/conversations/{}"
        token = "TOKEN=$(curl -s https://babeshop.ftech.ai/api/auth -d '{\"username\": \"me\", \"password\": \"w4J6OObi996nDGcQ4mlYNK4F\"}' | jq -r .access_token)"
        response = os.popen(token + " && " + conversation_detail_api.format(sender_id)).read()
        if response is not None:
            try:
                conversation_detail = json.loads(response)
                conversation_detail_list.append(conversation_detail)
            except Exception as ex:
                logger.error(ex)
    conversation_detail_df = pd.DataFrame(conversation_detail_list)
    return conversation_detail_df


def clean_rasa_chatlog(rasa_chatlog):
    logger.info("Clean rasa chatlog")

    rasa_chatlog["user_message_correction"] = rasa_chatlog["user_message"]
    rasa_chatlog = remove_col_str(df=rasa_chatlog, col_name="user_message_correction")
    rasa_chatlog = deEmojify(df=rasa_chatlog, col_name="user_message_correction", og_col_name="user_message_correction")
    rasa_chatlog = correction_message(df=rasa_chatlog, col_name="user_message_correction",
                                      og_col_name="user_message_correction")
    rasa_chatlog = remove_col_white_space(df=rasa_chatlog, col_name="user_message_correction")
    return rasa_chatlog


def get_last_document_from_db():
    client = MongoClient("mongodb+srv://ducanh:1234@ducanh.sa1mn.gcp.mongodb.net/<dbname>?retryWrites=true&w=majority")
    db = client['chatlog_db']
    collection = db[db_name]
    # last_document = [document for document in collection.find().limit(1).sort([('$natural', -1)])]
    # last_conversation_id = 0
    # if len(last_document) > 0:
    #     last_conversation_id = last_document[0]["conversation_id"]

    last_conversation_document = [document for document in collection.find({}, {'conversation_id': 1, '_id': 0}).sort(
        [('conversation_id', -1)]).limit(1)][0]
    last_conversation_id = last_conversation_document["conversation_id"]
    return last_conversation_id

def upload_to_db(df, database_name):
    # Connect to MongoDB
    client = MongoClient("mongodb+srv://ducanh:1234@ducanh.sa1mn.gcp.mongodb.net/<dbname>?retryWrites=true&w=majority")
    db = client['chatlog_db']
    collection = db[database_name]
    data_dict = df.to_dict("records")

    # Insert collection
    collection.insert_many(data_dict)


def get_raw_chatlog_from_db():
    client = MongoClient("mongodb+srv://ducanh:1234@ducanh.sa1mn.gcp.mongodb.net/<dbname>?retryWrites=true&w=majority")
    db = client['chatlog_db']
    collection = db[db_name]
    chatlog_df = pd.DataFrame([document for document in collection.find({})])
    return chatlog_df

def crawl_all():
    try:
        last_conversation_id = get_last_document_from_db()
    except:
        last_conversation_id = 0
    # all_conversations = export_conversations("2020-09-30", "2020-10-29")
    # all_conversation_detail = export_conversation_detail(all_conversations)
    # rasa_chatlog_processed = process_raw_rasa_chatlog(all_conversation_detail, None, ["2020"])
    #
    # upload_to_db(rasa_chatlog_processed, "oct_data_2")
    # rasa_chatlog_processed = get_raw_chatlog_from_db()

    rasa_chatlog_processed = pd.read_csv("chatlogs_bot_human/combined.csv")
    rasa_chatlog_processed = rasa_chatlog_processed[[True if datetime.datetime.strptime(x[:10], "%Y-%m-%d").weekday() <= 4 else False for x in rasa_chatlog_processed["created_time"]]]
    rasa_chatlog_clean = clean_rasa_chatlog(rasa_chatlog_processed)
    # rasa_chatlog_clean = rasa_chatlog_clean.sort_values(by=["created_time"])
    processor = RasaChalogProcessor()
    rasa_chatlog = processor.process_rasa_chatlog(rasa_chatlog_clean, last_conversation_id)
    upload_all_rasa_chatlog_to_atlas_mongodb(rasa_chatlog)

crawl_all()
