import sys

sys.path.append('.')
sys.path.append('../')
from chatlogs_bot_human.utils.helper import *
import numpy as np

def update_conv_uc_dict(conv_uc_dict: dict, begin_time, begin_date, conv_id, conversation_df):
    """
    Add item to conv_uc_dict
    :param conv_uc_dict:
    :return:
    """
    use_case_list = ["uc_s1", "uc_s2", "uc_s31", "uc_s32", "uc_s41", "uc_s42", "uc_s43", "uc_s51", "uc_s52", "uc_s53",
                     "uc_s8", "uc_s9", "other"]
    conv_uc_dict["conv_id"].append(conv_id)
    conv_uc_dict["conversation_begin_time"].append(begin_time)
    conv_uc_dict["conversation_begin_date"].append(begin_date)

    greet_first_turn = False
    last_turn = conversation_df["turn"].to_list()[-1]
    if conversation_df.iloc[0]["intent"] == "start_conversation" or conversation_df.iloc[0]["intent"] == "greet":
        greet_first_turn = True

    use_case_row = conversation_df[conversation_df["use_case"] != '']
    outcome = conversation_df[conversation_df["outcome"] != '']["outcome"].to_list()[0]
    sender_id = conversation_df["sender_id"].drop_duplicates().to_list()[0]
    conv_uc_dict["outcome"].append(outcome)
    conv_uc_dict["sender_id"].append(sender_id)
    if len(use_case_row) == 0:
        for item in use_case_list:
            if item != "other":
                conv_uc_dict[item].append(0)
            else:
                conv_uc_dict["other"].append(1)
        if last_turn > 0:
            if greet_first_turn:
                conv_uc_dict["_conv_id"].append(conversation_df[conversation_df["turn"] == 1].iloc[0]["_id"])
                conv_uc_dict["turn"].append(1)
            else:
                try:
                    if isinstance(conversation_df[conversation_df["turn"] == 0].iloc[0]["_id"], pd.Series):
                        a = 0
                    conv_uc_dict["_conv_id"].append(conversation_df[conversation_df["turn"] == 0].iloc[0]["_id"])
                except:
                    a = 0
                conv_uc_dict["turn"].append(0)
        else:
            conv_uc_dict["_conv_id"].append(conversation_df[conversation_df["turn"] == 0].iloc[0]["_id"])
            conv_uc_dict["turn"].append(0)
    else:
        conv_uc_dict["_conv_id"].append(use_case_row["_id"])
        conv_uc_dict["turn"].append(use_case_row["turn"].to_list()[0])
        use_case_name = use_case_row["use_case"].to_list()[0]
        for item in use_case_list:
            if item != use_case_name:
                conv_uc_dict[item].append(0)
            else:
                conv_uc_dict[use_case_name].append(1)

    return conv_uc_dict


def update_conv_outcome_dict(conv_outcome_dict: dict, begin_time, begin_date, conv_id, conversation_df):
    """
    Add item to conv_outcome_dict
    :param conv_outcome_dict:
    :return:
    """
    outcome_list = ["thank", "shipping_order", "handover_to_inbox", "silence", "other"]
    conv_outcome_dict["conv_id"].append(conv_id)
    conv_outcome_dict["conversation_begin_time"].append(begin_time)
    conv_outcome_dict["conversation_begin_date"].append(begin_date)

    outcome_row = conversation_df[conversation_df["outcome"] != '']
    use_case = conversation_df[conversation_df["use_case"] != ""]["use_case"].drop_duplicates().to_list()
    sender_id = conversation_df["sender_id"].drop_duplicates().to_list()[0]
    conv_outcome_dict["sender_id"].append(sender_id)
    if len(use_case) == 0:
        use_case = ["other"]
    conv_outcome_dict["use_case"].append(use_case)
    # if len(outcome_row) == 0:
    #     a = 0
    #
    # if not isinstance(outcome_row["_id"].to_list()[0], int):
    #     a = 0
    conv_outcome_dict["_conv_id"].append(outcome_row["_id"].to_list()[0])
    conv_outcome_dict["turn"].append(outcome_row["turn"].to_list()[0])
    outcome_name = outcome_row["outcome"].to_list()[0]
    for item in outcome_list:
        if item != outcome_name:
            conv_outcome_dict[item].append(0)
        else:
            conv_outcome_dict[outcome_name].append(1)
    return conv_outcome_dict


def upload_to_db(df, database_name):
    # Connect to MongoDB
    client = MongoClient("mongodb+srv://ducanh:1234@ducanh.sa1mn.gcp.mongodb.net/<dbname>?retryWrites=true&w=majority")
    db = client['chatlog_db']
    collection = db[database_name]
    data_dict = df.to_dict("records")

    # Insert collection
    collection.insert_many(data_dict)


def update_usecase_outcome_table(df=None):
    conv_uc_dict = {
        "_conv_id": [],
        "conv_id": [],
        "conversation_begin_date": [],
        "conversation_begin_time": [],
        "sender_id": [],
        "turn": [],
        "outcome": [],
        "uc_s1": [],
        "uc_s2": [],
        "uc_s31": [],
        "uc_s32": [],
        "uc_s41": [],
        "uc_s42": [],
        "uc_s43": [],
        "uc_s51": [],
        "uc_s52": [],
        "uc_s53": [],
        "uc_s8": [],
        "uc_s9": [],
        "other": [],
    }
    conv_outcome_dict = {
        "_conv_id": [],
        "conv_id": [],
        "conversation_begin_date": [],
        "conversation_begin_time": [],
        "sender_id": [],
        "turn": [],
        "use_case": [],
        "thank": [],
        "shipping_order": [],
        "handover_to_inbox": [],
        "silence": [],
        "other": [],
    }
    conv_ids = df["conversation_id"].drop_duplicates().to_list()

    for conv_id in conv_ids:
        conversation_df = df[df["conversation_id"] == conv_id]
        conversation_begin_time = conversation_df["conversation_begin_time"].drop_duplicates().to_list()[0]
        conversation_begin_date = conversation_df["conversation_begin_date"].drop_duplicates().to_list()[0]

        conv_uc_dict = update_conv_uc_dict(conv_uc_dict, conversation_begin_time, conversation_begin_date, conv_id,
                                           conversation_df)
        conv_outcome_dict = update_conv_outcome_dict(conv_outcome_dict, conversation_begin_time,
                                                     conversation_begin_date, conv_id, conversation_df)

    conv_uc_df = pd.DataFrame(conv_uc_dict)
    conv_uc_df = conv_uc_df.drop(columns=["_conv_id"])
    conv_outcome_df = pd.DataFrame(conv_outcome_dict)

    upload_to_db(conv_uc_df, "conversation_usecase_4")
    upload_to_db(conv_outcome_df, "conversation_outcome_4")
    return conv_uc_df, conv_outcome_df


def reformat_df_output_for_table(df: pd.DataFrame, collection_name):
    df = df.drop(columns=['_id','date', 'conversation_time', 'week_day', 'conversation_begin_date', 'conversation_begin_time', 'user_message_correction'])
    sub_df_list = []
    conversation_ids = df["conversation_id"].drop_duplicates().to_list()
    for conversation_id in conversation_ids:
        info_dict = {x: [] for x in list(df.columns)}
        info_dict.pop('turn', None)
        info_dict.pop('message_id', None)
        info_dict.pop('sender', None)
        info_dict.pop('attachments', None)
        user_counter = 0
        bot_counter = 0
        counter = 0
        conversation_outcome = []

        sub_df = df[df["conversation_id"] == conversation_id]
        sub_df["user_message"] = sub_df["user_message"].astype(str)
        sub_df["bot_message"] = sub_df["bot_message"].astype(str)
        sub_df = sub_df[(sub_df['user_message'] != sub_df['bot_message'])]
        if len(sub_df) == 0:
            continue
        try:
            conversation_uc = [x for x in sub_df["use_case"].drop_duplicates().to_list() if x != ''][0]
        except:
            conversation_uc = ""
        if "outcome" in sub_df:
            conversation_outcome = [x for x in sub_df["outcome"].drop_duplicates().to_list() if x != '']
            if conversation_outcome:
                conversation_outcome = conversation_outcome[0]
            else:
                conversation_outcome = ""

        sender_id = df[df["conversation_id"] == conversation_id]["sender_id"].drop_duplicates().to_list()[0]
        for row in sub_df.itertuples():
            counter += 1
            user_message = row.user_message
            bot_message = row.bot_message
            utter_name = row.utter_name
            if user_message is None or user_message == "None" or user_message == "":
                user_message = np.NaN
            if bot_message is None or bot_message == "None" or bot_message == "":
                bot_message = np.NaN
            if str(user_message) == str(bot_message) == "nan":
                continue
            if str(user_message) != "nan" and str(bot_message) == "nan":
                bot_counter = 0
                user_counter += 1

                info_dict["user_message"].append(user_message)
                info_dict["intent"].append(row.intent)
                info_dict["entities"].append(row.entities)
                info_dict["created_time"].append(row.created_time)

                if user_counter > 1:
                    info_dict["bot_message"].append(np.NaN)
                    info_dict["utter_name"].append(utter_name)
                    info_dict["created_time_bot"].append("")

                if counter == len(sub_df):
                    info_dict["bot_message"].append(np.NaN)
                    info_dict["created_time_bot"].append("")
                    info_dict["utter_name"].append(utter_name)


            elif str(user_message) == "nan" and str(bot_message) != "nan":
                bot_counter += 1
                user_counter = 0
                info_dict["utter_name"].append(utter_name)
                info_dict["bot_message"].append(bot_message)
                info_dict["created_time_bot"].append(row.created_time)

                if bot_counter > 1 or counter == 1:
                    info_dict["user_message"].append(np.NaN)
                    info_dict["created_time"].append("")
                    info_dict["intent"].append("")
                    info_dict["entities"].append("")

        dict_len = len(info_dict["user_message"])
        info_dict["conversation_id"] += [conversation_id] * dict_len
        info_dict["sender_id"] += [sender_id] * dict_len
        info_dict["use_case"].append(conversation_uc)
        info_dict["use_case"] += [""] * (dict_len - 1)
        if "outcome" in sub_df:
            info_dict["outcome"].append(conversation_outcome)
            info_dict["outcome"] += [""] * (dict_len - 1)
        try:
            # for index, item in info_dict.items():
            #     info_dict[index].append("")
            new_sub_df = pd.DataFrame.from_dict(info_dict)
        except:
            a = 0
        sub_df_list.append(new_sub_df)
    if sub_df_list:
        new_df = pd.concat(sub_df_list)
        upload_to_db(new_df, collection_name)
        return new_df
    return df


def main():
    df = get_chatlog_from_db("2020-01-02", "2020-10-19")
    conv_uc_df, conv_outcome_df = update_usecase_outcome_table(df)
    df.insert(list(df.columns).index("created_time") + 1, "created_time_bot", "")
    reformat_df_output_for_table(df, "rasa_chatlog_all_19_10_reformat")



main()
