from datetime import datetime
from chatlogs_bot_human.utils.helper import *
import datetime
import logging
import pandas as pd
import unicodedata
import sys

sys.path.append('.')
sys.path.append('../')
from define import conversations
from define import UC
from denver.models.flair_seq_tagger import FlairSequenceTagger

import os

script_dir = os.path.dirname(__file__)

logging.basicConfig(filename="logging_data/rasa_chatlog_processor_log",
                    format='%(asctime)s %(message)s',
                    filemode='w')
logger = logging.getLogger()
logger.setLevel(logging.INFO)
print(sys.path)

ner_model_path = script_dir + "/models/ApolloEntityExtractor_latest.pt"


class RasaChalogProcessor():
    def __init__(self):
        pass

    def specify_conversation_outcome(self, rasa_chatlog_df: pd.DataFrame):
        logger.info("Specify outcome for conversations")
        start_time = time.time()
        rasa_chatlog_df.insert(3, "outcome", "")
        conversation_ids = rasa_chatlog_df["conversation_id"].drop_duplicates(keep="first").to_list()

        key_words = ["ship", "gửi hàng", "lấy", "địa chỉ", "giao hàng", "đ/c", "thanh toán", "tổng", "stk",
                     "số tài khoản",
                     "gửi về"]
        filter_words = ["địa chỉ shop", "địa chỉ cửa hàng", "lấy rồi", "giao hàng chậm"]
        handover_bot_message = [
            "Dạ, bạn chờ trong ít phút shop kiểm tra kho hàng rồi báo lại bạn ngay ạ!",
            "Mời bạn bấm vào sản phẩm để xem thông tin chi tiết nhé",
            "Dạ, bạn vui lòng chờ trong ít phút nhân viên của shop sẽ phục vụ bạn ạ!^^",
            "/connect_employee",
        ]
        for id in conversation_ids:
            sub_conversation_df = rasa_chatlog_df[rasa_chatlog_df["conversation_id"] == id]
            sub_conversation_df = sub_conversation_df[[True if datetime.datetime.strptime(x[:10], "%Y-%m-%d").weekday() <= 4 else False for x in sub_conversation_df["created_time"]]]
            sub_conversation_df = sub_conversation_df.dropna(subset=["bot_message", "user_message"], how="all")
            try:
                last_turn = max(list(sub_conversation_df["turn"]))
            except:
                rasa_chatlog_df.loc[rasa_chatlog_df.conversation_id == id, "outcome"] = "other"
                continue
            last_turn_message_df = sub_conversation_df[sub_conversation_df["turn"] == last_turn]
            last_turn_message_df = last_turn_message_df.dropna(subset=["bot_message", "user_message"], how="all")
            message_counter = 0
            # has_outcome = False
            last_index = 0
            for index, item in last_turn_message_df.iterrows():
                user_message = item["user_message"]
                user_message_correction = False
                if str(user_message) != "nan":
                    user_message_correction = item["user_message_correction"]

                bot_message = item["bot_message"]
                user_intent = item["intent"]
                if str(user_intent) != "nan" and (user_intent == "thank" or any(x in user_message_correction for x in
                                                                                ["thanks", "thank", "tks", "cảm ơn",
                                                                                 "thankyou", "cám ơn"])):
                    rasa_chatlog_df.at[index, "outcome"] = "thank"
                    has_outcome = True
                    break
                elif user_message_correction and any(x in user_message_correction for x in key_words) and all(
                        x not in user_message_correction for x in filter_words):
                    rasa_chatlog_df.at[index, "outcome"] = "shipping_order"
                    has_outcome = True
                    break
                elif (str(user_intent) != "nan" and user_intent == "handover_to_inbox") or any(
                        x in str(bot_message) for x in handover_bot_message):
                    rasa_chatlog_df.at[index, "outcome"] = "handover_to_inbox"
                    has_outcome = True
                    break
                elif str(user_intent) != "nan" and user_intent == "agree":
                    # rasa_chatlog_df.at[index, "outcome"] = "agree"
                    rasa_chatlog_df.at[index, "outcome"] = "other"
                    has_outcome = True
                    break
                elif message_counter == (len(last_turn_message_df) - 1) and item["sender"] == "bot":
                    rasa_chatlog_df.at[index, "outcome"] = "silence"
                    has_outcome = True
                    break
                elif message_counter == (len(last_turn_message_df) - 1):
                    rasa_chatlog_df.at[index, "outcome"] = "other"
                    has_outcome = True
                    break
                message_counter += 1
                last_index = index


        logger.info("Specify outcomes: --- %s seconds ---" % (time.time() - start_time))
        return rasa_chatlog_df

    def remove_trash_conversation(self, chatlog_df: pd.DataFrame):
        successful_conversation = chatlog_df[chatlog_df["outcome"]]
        return chatlog_df

    def get_conversation_turn_without_and_with_image(self, chatlog_df: pd.DataFrame):
        start_time = time.time()

        conversation_turn_list_without_image = []
        conversation_turn_list_with_image = []
        conversation_ids = chatlog_df["conversation_id"].drop_duplicates(keep="first").to_list()
        for id in conversation_ids:
            sub_df = chatlog_df[chatlog_df["conversation_id"] == id]
            turns = sub_df["turn"].drop_duplicates(keep="first").to_list()
            # turns = [0, 1]
            for turn in turns:
                attachments = sub_df.loc[sub_df["turn"] == turn, "attachments"].dropna().to_list()
                attachments = [x for x in attachments if x != '']
                if len(attachments) == 0:
                    conversation_turn_list_without_image.append((id, turn))
                else:
                    conversation_turn_list_with_image.append((id, turn))
        logger.info("Get conversation turn without image: " + str(time.time() - start_time))
        return conversation_turn_list_without_image, conversation_turn_list_with_image

    def process_rasa_chatlog(self, df: pd.DataFrame, begin_converastion_id=0):
        """
        Processor
        :param input_month:
        :param raw_chatlog:
        :return:
        """
        start_time = time.time()
        logger.info("Start process chatlog")
        rasa_chatlog_by_month_df = df.dropna(subset=["bot_message", "user_message", "intent"], how="all")
        rasa_chatlog_by_month_df = rasa_chatlog_by_month_df[(rasa_chatlog_by_month_df["bot_message"] != '') & (rasa_chatlog_by_month_df["user_message"] != '') & (rasa_chatlog_by_month_df['intent'] != '')]

        model_ner = FlairSequenceTagger(mode="inference", model_path=ner_model_path)

        rasa_chatlog_by_month_df = conversations.split_chatlog_to_conversations(rasa_chatlog_by_month_df, begin_converastion_id)
        rasa_chatlog_by_month_df = conversations.split_chatlog_conversations_to_turns(rasa_chatlog_by_month_df)

        rasa_chatlog_by_month_df = UC.specify_uc1_uc2_uc3(rasa_chatlog_by_month_df)
        conversation_turn_pair_without_img, conversation_turn_pair_with_img = self.get_conversation_turn_without_and_with_image(rasa_chatlog_by_month_df)
        rasa_chatlog_by_month_df = UC.specify_uc4(rasa_chatlog_by_month_df, conversation_turn_pair_without_img, model_ner)
        rasa_chatlog_by_month_df = UC.specify_uc5(rasa_chatlog_by_month_df, conversation_turn_pair_without_img, model_ner)
        rasa_chatlog_by_month_df = UC.specify_uc8(rasa_chatlog_by_month_df, conversation_turn_pair_without_img)
        rasa_chatlog_by_month_df = UC.specify_uc9(rasa_chatlog_by_month_df, conversation_turn_pair_without_img)

        rasa_chatlog_by_month_df = self.specify_conversation_outcome(rasa_chatlog_by_month_df)

        # rasa_chatlog_by_month_df = self.remove_trash_conversation(rasa_chatlog_by_month_df)

        logger.info("Process rasa chatlog: --- %s seconds ---" % (time.time() - start_time))
        return rasa_chatlog_by_month_df
